const { inProduction } = require('../../env');
const options = require('../../options');

const { webpack: { analyse } } = options.get('paths');

const MiniCssExtractPlugin = require('./MiniCssExtractPlugin');
const TerserPlugin = require('./TerserPlugin');
const DefinePlugin = require('./DefinePlugin');
const ProgressPlugin = require('./ProgressPlugin');
const BundleAnalyzerPlugin = require('./BundleAnalyzerPlugin');

const prodPlugins = [TerserPlugin, MiniCssExtractPlugin];

const defaultPlugins = [DefinePlugin, ProgressPlugin];

if (analyse) {
    defaultPlugins.push(BundleAnalyzerPlugin);
}

module.exports = inProduction() ? [...defaultPlugins, ...prodPlugins] : defaultPlugins;
