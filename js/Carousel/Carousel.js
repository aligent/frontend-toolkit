define(['jquery'], ($) => {
    /**
     * Class Carousel
     *
     * For documentation, see `docs/js/Carousel.md`
     */
    class Carousel {

        /**
         * Constructor
         *
         * @param {object}  config  Configuration options passed in through JSON
         * @param {Element} element The element that this class was bound to
         *
         * @returns {undefined}
         */
        constructor(config, element) {
            const defaultConfig = {};
            // Merge default config options and provided config
            this.config = { ...defaultConfig, ...config };
            this.$element = $(element);

            this.prevBtnSelector = this.config.prevBtn || '.carousel-prev';
            this.nextBtnSelector = this.config.nextBtn || '.carousel-next';
            this.itemsSelector = this.config.items || '.carousel-items';

            this._attachListeners();

            if (this.config.autoplay) {
                this.interval = this.startAutoplay();
            }
        }

        /**
         * Set up the timing for autoplay of the carousel
         *
         * @returns {number} The ID of the interval created
         */
        startAutoplay() {
            const delay = this.config.delay || 5000;

            return window.setInterval(() => {
                this.carouselToggleNext();
            }, delay);
        }

        /**
         * Remove the old intervale and start a new one
         * This is required if a user changes to the next carousel item themselves, the timer should reset
         *
         * @returns {undefined}
         */
        restartAutoplay() {
            if (this.interval) {
                window.clearInterval(this.interval);
            }

            this.interval = this.startAutoplay();
        }

        /**
         * Attach event listeners to elements
         *
         * @private
         * @returns {undefined}
         */
        _attachListeners() {
            this.$element
                .on('click', this.prevBtnSelector, () => {
                    if (this.config.autoplay) {
                        this.restartAutoplay();
                    }

                    this.carouselToggleNext(false);
                })
                .on('click', this.nextBtnSelector, () => {
                    if (this.config.autoplay) {
                        this.restartAutoplay();
                    }

                    this.carouselToggleNext(true);
                });
        }

        /**
         * Toggle the carousel to the next element
         *
         * @param {boolean} showNext [Optional] Provide false if you wish the carousel to go to the previous element
         *
         * @returns {undefined}
         */
        carouselToggleNext(showNext = true) {
            const $items = this.$element.find(this.itemsSelector);
            const $current = $items.find('li.carousel-item-active'); // Get currently visible item
            let $next; // The next carousel item to show, not necessarily the next item in the DOM

            if (showNext) {
                $next = $current.next('li'); // Get next list item
            } else {
                $next = $current.prev('li'); // Get previous list item
            }

            if ($next.length === 0) {
                $next = showNext ? $items.find('li:first-child') : $items.find('li:last-child');
            }

            const nextImages = $next.find('img[data-src]');
            nextImages.each((index, image) => {
                const $image = $(image);
                if (!$image.prop('src')) {
                    $image.prop('src', $image.data('src'));
                }
            });

            this._throwEvent('before_aligent_carousel_toggle');

            this._throwEvent('before_aligent_carousel_hide_prev', {
                previous: $current
            });
            $current.toggleClass('carousel-item-active', false);
            this._throwEvent('after_aligent_carousel_hide_prev', {
                previous: $current
            });

            this._throwEvent('before_aligent_carousel_show_next', {
                next: $next
            });
            $next.toggleClass('carousel-item-active', true);
            this._throwEvent('after_aligent_carousel_show_next', {
                next: $next
            });

            this._throwEvent('after_aligent_carousel_toggle');
        }

        /**
         *
         * @param {string} event Name of event to throw
         * @param {object} extraParams [Optional] Extra parameters to send with the event
         *
         * @private
         * @returns {undefined}
         */
        _throwEvent(event, extraParams = {}) {
            this.$element.trigger(event, extraParams);
        }

    }

    return (config, element) => {
        const carousel = new Carousel(config, element);
        return carousel;
    };
});
