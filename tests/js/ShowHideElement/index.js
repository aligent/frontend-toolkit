/*eslint-disable*/
const { expect } = require('chai');

describe('ShowHideElement', () => {
    let page;

    /**
     * Before each test, load the example page in a new tab
     */
    before (async () => {
        page = await browser.newPage();
        await page.goto('http://127.0.0.1:8080/js/ShowHideElement/example.html');
    });

    /**
     * Close the page after each test has completed
     */
    after (async () => {
        await page.close();
    });

    it('should add visibility class name', async () => {
        const toggleButton = await page.$('.toggle-basic .btn__toggle');
        await toggleButton.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const container = document.querySelector("#toggle-basic-container");
            return container.className.split(' ');
        });

        expect('show-hide--toggle').to.be.oneOf(classNames);
    });

    it('should remove visibility class name', async () => {
        // Set up the page so the JS class can remove the class name from the container
        await page.evaluate(() => {
            document.querySelector("#toggle-basic-container").classList.add('show-hide--toggle');
        });

        const toggleButton = await page.$('.toggle-basic .btn__toggle');
        await toggleButton.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const container = document.querySelector("#toggle-basic-container");
            return container.className.split(' ');
        });

        expect('show-hide--toggle').to.not.be.oneOf(classNames);
    });

    it('should toggle class on self', async () => {
        const toggleButton = await page.$('.toggle-self .btn__toggle');
        await toggleButton.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const button = document.querySelector('.toggle-self .btn__toggle');
            return button.className.split(' ');
        });

        expect('show-hide--toggle').to.be.oneOf(classNames);
    });

    it('should toggle class on parent', async () => {
        const toggleButton = await page.$('.toggle-parent .btn__toggle');
        await toggleButton.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const button = document.querySelector('.toggle-parent .toggle-parent-container:first-of-type');
            return button.className.split(' ');
        });

        expect('show-hide--toggle').to.be.oneOf(classNames);

        const classNames1 = await page.evaluate(() => {
            const button = document.querySelector('.toggle-parent .toggle-parent-container:last-of-type');
            return button.className.split(' ');
        });

        expect('show-hide--toggle').to.not.be.oneOf(classNames1);
    });

    it('should toggle class on child', async () => {
        const toggleElement = await page.$('.toggle-child article:first-of-type');
        await toggleElement.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const toggledElement = document.querySelector('.toggle-child-container article:first-of-type section');
            return toggledElement.className.split(' ');
        });

        expect('show-hide--toggle').to.be.oneOf(classNames);

        const classNames1 = await page.evaluate(() => {
            const untoggledElement = document.querySelector('.toggle-child-container article:last-of-type section');
            return untoggledElement.className.split(' ');
        });

        expect('show-hide--toggle').to.not.be.oneOf(classNames1);
    });

    it('should toggle class on a sibling element', async () => {
        const toggleElement = await page.$('.toggle-sibling article:first-of-type header');
        await toggleElement.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const toggledElement = document.querySelector('.toggle-sibling-container article:first-of-type section');
            return toggledElement.className.split(' ');
        });

        expect('show-hide--toggle').to.be.oneOf(classNames);

        const classNames1 = await page.evaluate(() => {
            const untoggledElement = document.querySelector('.toggle-sibling-container article:last-of-type section');
            return untoggledElement.className.split(' ');
        });

        expect('show-hide--toggle').to.not.be.oneOf(classNames1);
    });
});
