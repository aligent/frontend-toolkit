const glob = require('glob');
const path = require('path');
const fs = require('fs');
const nconf = require('nconf');

const config = path.resolve('./toolkitConfig.json');
if (!fs.existsSync(config)) {
    console.info('No project config defined at', config);
}

nconf
    .use('memory')
    .env({ lowerCase: true })
    .argv({
        mode: {
            alias: 'deploy',
            default: 'production'
        }
    })
    .file(config)
    .defaults({
        projectUrl: false,

        verbose: false,

        tasks: {
            scss: true,
            javascript: true,
            react: true,
            copy: true,
            fonts: false,
            images: false,
            browserSync: false,
            icons: false,
            cache: false,
            styleguide: false
        },

        sassCompiler: 'dart',

        envEntryCmd: './vessel',

        themeBaseDirs: glob.sync(`${process.cwd()}/app/design/frontend/**/source`),

        mode: 'production',

        iconFontName: 'aligent-icons',

        sassOptions: {
            includePaths: [
                ...glob.sync(`${process.cwd()}/app/design/frontend/**/source/{sass,scss}`), // find all source/sass dirs
                `${process.cwd()}/node_modules` // NPM Dependencies
            ]
        },

        paths: {
            base: './app/design/frontend',
            src: {
                js: ['./app/design/frontend/**/source/js/**/*.js'],
                scss: ['./app/design/frontend/**/source/{sass,scss}/**/*.scss'],
                images: ['./app/design/frontend/**/source/images/**/*.*'],
                icons: ['./app/design/frontend/**/source/icons/**/*.svg'],
                fonts: ['./app/design/frontend/**/source/fonts/**/*.*'],
                translations: ['./app/**/i18n/**/*.csv'],
                layouts: ['./app/design/frontend/**/*.xml'],
                blocks: ['./app/**/templates/**/*.phtml'],
                copy: ['./app/design/frontend/**/source/{fonts,images,icons}/**/*.*']
            },

            dest: {
                regex: '(source)',
                replacement: 'web',
                base: './app/design/frontend',
                icons: './app/design/frontend/**/source/scss/icons/_icons.scss',
                serviceWorker: './app/design/frontend/Aligent/base/web/js/sw.js',
                styleguide: 'styleguide',

            },

            webpack: {
                analyse: false,
                entry: {
                    'react-checkout': glob.sync(`${process.cwd()}/app/design/frontend/**/base/source/react/Checkout/index.js`, { nocase: true }),
                    'react-plp': glob.sync(`${process.cwd()}/app/design/frontend/**/base/source/react/Plp/index.js`, { nocase: true })
                },
                output: {
                    dest: glob.sync(`${process.cwd()}/app/design/frontend/**/base/web/`, { nocase: true })[0],
                    js: { filename: 'js/[name].js' },
                    css: { filename: 'css/[name].css' }
                },
                resolve: {
                    modules: glob.sync(`${process.cwd()}/app/design/frontend/**/source`),
                    alias: {
                        ROOT: `${process.cwd()}`,
                        VENDOR: 'ROOT/vendor'
                    }
                },
                sassUrlRoot: glob.sync(`${process.cwd()}/app/design/frontend/**/base/source/{sass,scss}`, { nocase: true })[0],
                componentOverrideMapping: ''
            },

            browserSync: `${process.cwd()}/app/design/**/*.phtml`
        },

        babelConfig: {
            configFile: false,
            presets: [['@babel/preset-env', { modules: false }]],
            plugins: [
                '@babel/plugin-transform-async-to-generator',
                '@babel/plugin-proposal-object-rest-spread',
                '@babel/plugin-proposal-class-properties',
                '@babel/plugin-syntax-dynamic-import'
            ]
        },
        splitChunksConfig: {
            commons: {
                test: '[\/]node_modules[\/](axios|react|react-dom|redux|react-redux|redux-thunk|aligentreact|reselect|classnames|lodash|core-js|prop-types|object-assign|intersection-observer|path-to-regexp|query-string|debounce-promise|regenerator-runtime)[\/]',
                name: 'vendor',
                chunks: 'all',
                enforce: true
            }
        }
    });

module.exports = nconf;
