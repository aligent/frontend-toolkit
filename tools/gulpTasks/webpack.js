/* eslint-disable import/no-extraneous-dependencies */
const { src, dest } = require('gulp');
const _ = require('lodash');
const webpack = require('webpack');
const plumber = require('gulp-plumber');
const webpackStream = require('webpack-stream');

const options = require('../options');
const webpackConfig = require('../webpack');

module.exports = function buildWebpack(done) {
    const paths = options.get('paths');
    const destPath = options.get('paths').webpack.output.dest;
    const tasks = options.get('tasks');
    const verbose = options.get('verbose');
    const splitChunksConfig = options.get('splitChunksConfig');

    if (verbose) {
        console.log('Paths:', paths.webpack);
        console.log('Webpack Analyse On:', paths.webpack.analyse);
        console.warn('React Task Enabled:', tasks.react);
        console.log('React options:', webpackConfig());
        console.log('SplitChunks options:', splitChunksConfig);
    }
    if (!tasks.react) {
        return done();
    }

    const entries = _.pickBy(paths.webpack.entry, v => v.length > 0);
    if (_.isEmpty(entries)) {
        console.warn('No entry points supplied, Check your paths.', paths.webpack.entry);
        return done();
    }
    // webpackConfig entry points override gulp src paths
    return src(paths.src.js)
        .pipe(plumber())
        .pipe(webpackStream(webpackConfig(), webpack))
        .pipe(dest(destPath))
        .on('end', done);
};
