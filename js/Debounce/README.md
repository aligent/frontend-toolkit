# Debounce

Returns a function, that, as long as it continues to be invoked, will not be triggered.
The function will be called after it stops being called for N milliseconds.

See https://davidwalsh.name/javascript-debounce-function for more information

## Parameters

* **func**     {function} The function to debounce/throttle
* **wait**     {int}      The amount of time, in ms, to wait between executing `func`
* **immedite** {Boolean}  [Optional] If true, trigger the function on the leading edge, instead of the trailing. Default: false

## Usage

This class is only intended to be used inside other classes, therefore loaded as a dependency

```javascript
define(['debounce'], function(debounce) {

    $(document).on('scroll', debounce(() => {
        // Perform operations like checking for sticky header
    }, 50));

})
```

## Events

This class doesn't throw any events
