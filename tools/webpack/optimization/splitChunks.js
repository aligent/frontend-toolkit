const options = require('../../options');

const splitChunksConfig = options.get('splitChunksConfig');

const groups = Object.entries(splitChunksConfig).reduce((config, [key, value]) => {
    const test = `${value.test}`;
    const regexTest = new RegExp(test);
    return {
        ...config,
        [key]: { ...value, test: regexTest }
    };
}, {});

module.exports = {
    splitChunks: {
        cacheGroups: groups
    }
};
