const resolve = require('../webpack/resolver');
const cssRules = require('../webpack/rules/cssRules');
const fontRules = require('../webpack/rules/fontRules');
const imageRules = require('../webpack/rules/imageRules');
const tsRules = require('../webpack/rules/tsRules');

// Export a function. Accept the base config as the only param.
module.exports = ({ config, mode }) => {
    return Object.assign({}, config, {
        resolve,
        module: {
            rules: [
                // slightly modified babel.config rules
                {
                    test: /\.jsx?$/,
                    exclude: /\.ejs$/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: [['@babel/preset-env', { shippedProposals: true, modules: false }], '@babel/preset-react'],
                            plugins: [
                                '@babel/plugin-transform-async-to-generator',
                                '@babel/plugin-proposal-object-rest-spread',
                                '@babel/plugin-proposal-class-properties',
                                '@babel/plugin-syntax-dynamic-import'
                            ]
                        }
                    }
                },
                tsRules,
                cssRules,
                fontRules,
                imageRules
            ]
        }
    });
};
