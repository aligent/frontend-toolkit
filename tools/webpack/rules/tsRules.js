module.exports = {
    test: /\.tsx?$/,
    use: [
        {
            loader: 'babel-loader?babelrc',
            options: {
                babelrc: true,
                cacheDirectory: true
            }
        },
        {
            loader: 'ts-loader'
        }
    ]
};
