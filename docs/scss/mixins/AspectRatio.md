# Aspect Ratio

`aspect-ratio($width, $height)`, where $width and $height are the starting dimensions or a ratio, e.g. $width: 16, $height: 9

A mixin to help maintain the aspect ratio of an element. Especially useful when dealing with videos.

Mixin gets applied to a container for the actual element, so the following markup must be followed.

For more information, refer to original CSSTrick article, [https://css-tricks.com/snippets/sass/maintain-aspect-ratio-mixin/](https://css-tricks.com/snippets/sass/maintain-aspect-ratio-mixin/)

## Usage

```html
<div class="container">
    <video><video>
</div>
```

```css
.container {
  @include aspect-ratio(16, 9);
}
```

