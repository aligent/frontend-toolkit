# Example Component Override Project Structures

This example shows the different ways that we can use the FE Toolkit's component override pattern in hybrid Magento themes

## Component Override

Explore the directory structure in `example-component-override-project-structure`;

In this example, we have two themes, `Aligent/stamford` and `Aligent/wright`. `stamford` will be our "base" theme.
 
The `stamford` theme has 2 components, App and Loading. The Loading component contains some information specific to `stamford`, so that is overridden and updated in `wright`.

The `wright` theme also has a different `primary-colour`, which is a purple rather than a blue. This is achieved through the `themeSettings` override.
