const { series } = require('gulp');
const chalk = require('chalk');
const options = require('./tools/options');
const tasks = require('./tools/gulpTasks');
const { version } = require('./package.json');

console.log(chalk`
    Running FE-ToolKit {white.bgBlue ${version}} in {black.bgYellow ${options.get('mode')}} mode.
`);

module.exports = {
    version,
    tasks,
    options,
    testGulp: series(tasks.scss)
};
