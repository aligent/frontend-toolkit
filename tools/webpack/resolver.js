const options = require('../options');

const paths = options.get('paths');

module.exports = {
    modules: [
        ...paths.webpack.resolve.modules,
        'node_modules'
    ],
    alias: paths.webpack.resolve.alias,
    extensions: ['.js', '.jsx', '.json', '.webpack.js', '.web.js', '.css', '.sass', '.scss', '.ts', '.tsx']
};
