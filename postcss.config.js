module.exports = ({ env }) => ({
    modules: true,
    plugins: [
        require('autoprefixer'),
        env !== 'production' ? false : require('cssnano')
    ]
});
