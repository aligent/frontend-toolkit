const nconf = require('nconf');
const workboxBuild = require('workbox-build');

module.exports = function generateSWTask(done) {
    const paths = nconf.get('paths');

    return workboxBuild
        .generateSW({
            globDirectory: paths.dest.base,
            globPatterns: ['**/web/**/*.{js,css,eot,woff,woff2,ttf,svg,png}'],
            swDest: paths.dest.serviceWorker,
            clientsClaim: true,
            skipWaiting: true,
            manifestTransforms: [
                // Basic transformation to remove a certain URL:
                (originalManifest) => {
                    const manifest = originalManifest.map(entry => entry.url.replace(/.*\/web/g, '..'));
                    // Optionally, set warning messages.
                    const warnings = [];
                    return { manifest, warnings };
                }
            ]
        })
        .then(({ warnings }) => {
            // In case there are any warnings from workbox-build, log them.
            for (const warning of warnings) {
                console.warn(warning);
            }
            console.info('Service worker generation completed.');
        })
        .catch((error) => {
            console.warn('Service worker generation failed:', error);
        })
        .then(done);
};
