const { src, dest } = require('gulp');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');
const nconf = require('nconf');

module.exports = function buildImages(done) {
    const paths = nconf.get('paths');
    const tasks = nconf.get('tasks');
    const verbose = nconf.get('verbose');
    if (verbose) {
        console.log('Paths:', paths.src.images);
        console.warn('Image Task Enabled:', tasks.images);
    }
    if (!tasks.images) {
        return done();
    }
    return src(paths.src.images, { base: paths.base })
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(
            rename((path) => {
                // Replace the source paths with destination ones
                path.dirname = path.dirname.replace(new RegExp(paths.dest.regex), paths.dest.replacement);
                if (verbose) {
                    console.log(`Moving ${path.basename}${path.extname} to ${path.dirname}`);
                }
            })
        )
        .pipe(dest(paths.dest.base));
};
