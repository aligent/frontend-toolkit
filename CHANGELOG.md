# 4.1.0

- Simplified the component override pattern. Now all path processing is done using a utility function in the FE Toolkit. See updated `componentOverrideMapping.sample.js`

# 4.0.0

- Added ability to use component overrides
- Updated required NodeJS engine version to 12.*.*

# 3.3.3

- Get webpack dest path directly, fixes issue with dynamic destinations

# 3.3.2

- Add graphql webpack resolver

# 3.3.1

- Fix excludes for tsconfig
- Fix webpack resolver for tsx files
- Add jsx react config
- Update TS target to es2020
- Disable strictNullChecks set to false

# 3.3.0

- Upgrade to latest node packages
- Update webpack sass-loader to use dart-sass
- Add sassCompiler option to enable back to node-sass for scss gulp task
- Fix js linting
- Add pipeline tests for scss, js, and gulp

# 3.2.8

- Fix sass rems() function so compatible with calculations

# 3.2.7

- Update scss gulp task to use canonical dart-sass for better feature coverage

# 3.2.6

- Add Timer JS module for creating/managing timers in JS across multiple modules

# 3.2.5

- Fix ts file support
- Add support for setting `path.base` on tasks if you required source to be else where from `app/design/frontend`
- Update packages

# 3.2.4

- Add react testing packages, jest, enzyme, etc
- Fix issue with old stylelint config
- Fix issue where gulp babel was still inheriting from root babel.config.js
- Fix issue with issue with webpack sass imports being case sensitive
- Update docs with correct @aligent packages

# 3.2.3

- Fix configs for eslint, babel and stylelint.
- Upgrade packages

# 3.2.2

- move to @aligent/stylelint-config

# 3.2.1

- move to @aligent/eslint-config-aligent

# 3.2.0

- Add feature to use webpack-bundle-analyser plugin with `paths.webpack.analyse` flag
- Add option to config webpacks splitChunks optimisation plugin
- Bump eslint config 2.1.3 - supports security linting
- Fix task option calls

# 3.1.1

- Fix 🐛 webpack progress plugin for support of headless docker builds

# 3.1.0

- Fix paths for storybook
- Add copy task - to handle images, icons and fonts in source/ folder.
- Better support for custom paths in tasks
- Better scss minification by default
- Fix babel config

# 3.0.0

- Upgrade to Gulp 4
- Upgrade webpack 4
- Separate gulp tasks
- Improved config

# 2.8.1

Added exclude of text_assets in react build

# 2.8.0

Add rounded-table-borders mixin

# 2.7.0

Added SCSS mixins

- background-svg
- pseudo-icon

and function

- fetoolkit-get-icon-path

# 2.6.10

When building for production, tasks now exit with a non-zero error code if they
fail.

# 2.6.9

The react gulp task will now build to the folder set for react, not the folder set for JS

# 2.6.8

Font task won't run unless a src is specified

# 2.6.7

Added Lazyload feature to Carousel. It will check if the carousel item has images with data-src
and set the data-src value to src property before showing the item

# 2.6.6

Custom fonts now work in styleguide.
New task 'fonts' created that copies fonts from specified source to destination, and also to the styleguide.
There is no watch task for this so it will only run on build.

# 2.6.5

# 2.6.4

ESLint updates to disable closing object curly brace having to be on a new line, if the opening one is already on the same line

# 2.6.3

ESLint updates to work Jest tests

# 2.6.2

ESLint updates to work with modern React codebases

# 2.6.1

Updated React source glob in the gulpfile to ignore test files from being added to the bundle

# 2.6.0

- Updated Toolkit to be compatible with any project, as source, destination, outputFilenames and tasks can now
  all be specified individually
- Styleguide is now optional, disabled by default
- Updated README to include a reference of all possible options

# 2.5.0

Added React build task

# 2.4.0

Changed the name of the package to Frontend Toolkit

# 2.3.0

Added media query mixin for detecting high DPI devices in CSS, allowing higher resolution images to be used

# 2.2.0

Updated npm linting scripts to only show error message when there is an error

Updated normalize stylesheet to set input font family to inherit, rather than the opiniated `sans-serif`

Added new mixin/function combo for setting font size in `rem` using `px`

# 2.1.2

Added a JS Proxy for the options in the gulpgile so that users are informed if they didn't pass through a required parameter

# 2.1.1

File names of JS classes need to remain verbose, as naming them `index.js` will mean that transpiling the class into the
web directory of Magento, the filename will be index.js, which is going to conflict with all the other classes as they
get built.

# 2.1.0

Grouped related files for each JS class

# 2.0.1

Moved eslint packages to dependencies list rather than just dev dependencies

Updated README to be a little clearer about setting up eslint

# 2.0.0

Created the test framework

Also wrote tests for the ShowHideElement class

# 1.6.1

Disabled the `class-methods-use-this` ESLint rule

# 1.6.0

Added a Debounce JS class

# 1.5.2

Fixed the reset-anchor() mixin so that colour and text-transform aren't "reset" for states of the input, only outline

# 1.5.1

Actually imported the new reset-button() mixin so that it can be used

# 1.5.0

Specified options for the SASS Gulp plugin which defines paths to include from

This means that to include a file from the node_modules folder, the path no longer has to be relative to the file
that is doing the importing

Added an extra npm run script, that will run both linters, one after the other

Added a button-reset scss mixin

# 1.4.0

Updated expected class name pattern to allow for BEM class names

Also added missing stylelint.rc rule to the linters/stylelint/index.js file

# 1.3.1

Fixed the placeholder mixin so that it would attach the pseudo selectors to the parent element, as should be the case

# 1.3.0

Added more SCSS mixins that were missed during initial import:

- reset-ul
- reset-anchor
- css-arrow
- flex-column

# 1.2.0

Added a helper style rule for the skip to content link that is only meant for screen readers

# 1.1.1

README fixes and new stylelint rule for spaces in comments

# 1.1.0

Added SCSS mixins:

- aspect-ratio
- clear
- fix-whitespace
- placehoder

Implemented Stylelint

Added to README instructions on using the linters (ESLint and Stylelint) in M2 projects that this toolkit is included in

# 1.0.0

Initial release!

Includes the gulp task runner file and 4 RequireJS modules:

- Carousel
- RestrictHeight
- ShowHideElement
- ToggleTabElements
