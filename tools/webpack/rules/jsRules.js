module.exports = {
    test: /\.jsx?$/,
    use: {
        loader: 'babel-loader?babelrc',
        options: {
            babelrc: true,
            cacheDirectory: true
        }
    }
};
