# CSS Arrow

Create an arrow using CSS border and transform rotation

`css-arrow($direction, $size, $thickness)`, where

* *$direction* up/right/down/left
* *$size* The size of the arrow. Arrow will be displayed as a square, so width and height at both set to $size
* *$thickness* Thickness of the line

## Usage

```html
<div class="arrow-right"></div>
```

```css
.arrow-right {
  &::after {
    @include css-arrow(right, 50px, 4px);
  }
}
```
