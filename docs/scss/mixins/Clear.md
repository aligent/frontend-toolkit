# Clear

`clear($element)`, where `$element` is `'element'` or `'pseudo'`

* `'element` - clear the float directly on the element that the mixing is included on
* `'pseudo'` - clear the float using an :after pseudo element

## Usage


### clear('element')
```css
.element {
  @include clear('element');
}
```

*Result:*

```css
.element {
  clear: both;
}
```

### clear('pseudo')

```css
.element {
  @include clear('pseudo');
}
```

*Result:*

```css
.element {
  &:after {
    content: '';
    display: block;
    clear: both;
  }
}
```
