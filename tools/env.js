const nconf = require('nconf');

const inProduction = () => nconf.get('NODE_ENV') === 'production' || nconf.get('mode') === 'production';

const mode = () => inProduction() ? 'production' : 'development';

module.exports = {
    mode,
    inProduction
};
