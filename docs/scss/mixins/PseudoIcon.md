# Pseudo Icon

A mixin that will set up an element to take a pseudo icon
That means this mixin will usually be used in conjunction with an icon-*() mixin

`pseudo-icon($width: $height: $width)`, where `$width` and `$height` are the desired dimensions of the icon.



### Options

#### $width
*CSS Length unit*

The width for the icon
**@param** ``

#### $height
*CSS Length unit*

\[Optional\] The height of the icon. Default will be the same as $width, therefore a square

## Usage

```css
button {
  @include pseudo-icon(50px);
  @include icon-minicart();
}

// Where
@mixin icon-minicart() {
  // Path to image is `app/design/frontend/{Vendor}/{ThemeName}/web/images/icon/icon-minicart.svg`
  @include background-svg('icon-minicart'); // mixin located in FE Toolkit
}
```

