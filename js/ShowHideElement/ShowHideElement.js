/* global jQuery */
define(['jquery'], ($) => {
    /**
     * Class ShowHideElement
     *
     * For documentation, see `docs/js/ShowHideElement.md`
     */
    class ShowHideElement {

        /**
         * Constructor
         *
         * @param {object}  config  Configuration options passed in through JSON
         * @param {Element} element The element that this class was bound to
         *
         * @returns {undefined}
         */
        constructor(config, element) {
            const defaultConfig = {
                elementDirection: '',
                elementToToggle: ''
            };
            // Merge default config options and provided config
            this.config = { ...defaultConfig, ...config };
            this.$element = $(element);

            this.$elementToToggle = this.selectToggleElement();
            this.attachListener();
        }

        /**
         * Select the element that will have the show/hide class toggled on it
         *
         * @returns {jQuery}
         */
        selectToggleElement() {
            // If an elementDirection isn't set, default to searching from the global document
            if (typeof this.config.elementDirection === 'undefined') {
                return $(this.config.elementToToggle);
            }

            // If toggling class-name on the same element that instantiated this class
            if (this.config.elementDirection === 'self') {
                return this.$element;
            }

            // If the element to toggle is a child
            if (this.config.elementDirection === 'child') {
                return this.$element.find(this.config.elementToToggle);
            }

            // If the element to toggle is a parent
            if (this.config.elementDirection === 'parent') {
                return this.$element.closest(this.config.elementToToggle);
            }

            // If the element to toggle is a sibling
            if (this.config.elementDirection === 'sibling') {
                return this.$element.parent().find(this.config.elementToToggle);
            }

            // If none of the above matches, then default to selecting the element using document
            return $(this.config.elementToToggle);
        }

        /**
         * Attach event listener to element
         *
         * @returns {undefined}
         */
        attachListener() {
            this.$element.on('click', (e) => {
                e.preventDefault();
                this.toggleElement();
            });
        }

        /**
         * Toggle the class on the element which indicates if it is visible or not
         *
         * @returns {undefined}
         */
        toggleElement() {
            this.throwEvent('before_aligent_showhide_toggle');

            if (this.$elementToToggle.hasClass('show-hide--toggle')) {
                this.throwEvent('before_aligent_showhide_hide');
                this.$elementToToggle.removeClass('show-hide--toggle');
                this.throwEvent('after_aligent_showhide_hide');
            } else {
                this.throwEvent('before_aligent_showhide_show');
                this.$elementToToggle.addClass('show-hide--toggle');
                this.throwEvent('after_aligent_showhide_show');
            }

            this.throwEvent('after_aligent_showhide_toggle');
        }

        /**
         * Throw an event that can be used to hook into the show/hide action
         *
         * @param {string} eventName The name of the event to throw
         *
         * @returns {undefined}
         */
        throwEvent(eventName) {
            this.$elementToToggle.trigger(eventName);
        }

    }

    return (config, element) => {
        const showHideElement = new ShowHideElement(config, element);
        return showHideElement;
    };
});
