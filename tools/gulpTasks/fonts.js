const { src, dest } = require('gulp');
const nconf = require('nconf');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');

const { inProduction } = require('../env');

/**
 * Copy Font source files
 *
 * This will also create a copy of the fonts folder in the styleguide directory
 */
module.exports = function buildFonts(done) {
    const paths = nconf.get('paths');
    const tasks = nconf.get('tasks');
    const verbose = nconf.get('verbose');
    if (!tasks.fonts) {
        console.log('Fonts Task disabled');
        return done();
    }

    src(paths.src.fonts, { base: paths.base })
        .pipe(plumber())
        .pipe(
            rename((path) => {
                // Replace the source paths with destination ones
                path.dirname = path.dirname.replace(new RegExp(paths.dest.regex), paths.dest.replacement);
                if (verbose) {
                    console.log(`Moving ${path.basename}${path.extname} to ${path.dirname}`);
                }
            })
        )
        .pipe(dest(paths.dest.base));

    if (!inProduction() && tasks.styleGuide) {
        // Copy the fonts directory from the theme folder to the styleguide folder
        src(paths.src.fonts).pipe(dest(`${paths.dest.styleguide}/fonts`));
    }

    return done();
};
