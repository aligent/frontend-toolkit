/* eslint-disable no-param-reassign */
const { src, dest } = require('gulp');
const gulpSass = require('gulp-sass');
const fiber = require('fibers');
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');
const gulpIf = require('gulp-if');
const postcss = require('gulp-postcss');
const dartSass = require('sass');
const nodeSass = require('node-sass');

const options = require('../options');
const { inProduction } = require('../env');

module.exports = function buildSass(done) {
    const sassCompiler = options.get('sassCompiler');
    gulpSass.compiler = sassCompiler === 'dart'
      ? dartSass
      : nodeSass;

    const paths = options.get('paths');
    const tasks = options.get('tasks');
    const verbose = options.get('verbose');
    const sassOptions = options.get('sassOptions');
    if (verbose) {
        console.log('Sass compiler:', sassCompiler);
        console.log('Paths:', paths.src.scss);
        console.warn('Scss Task Enabled:', tasks.scss);
        console.warn('StyleGuide MODE Enabled:', tasks.styleGuide);
        console.log('Scss options:', sassOptions);
        console.log('Styleguide css path:', `${paths.dest.styleguide}/css`);
    }
    if (!tasks.scss) {
        return done();
    }

    return src(paths.src.scss, { base: paths.base })
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(
            gulpSass({ ...sassOptions, fiber }).on('error', function sassError(error) {
                console.error(error.toString());
                return inProduction() ? process.exit(1) : this.emit('end');
            })
        )
        .pipe(postcss())
        .pipe(
            rename((path) => {
                // Replace the source paths with destination ones
                path.dirname = path.dirname.replace(new RegExp(paths.dest.regex), paths.dest.replacement);
                path.dirname = path.dirname.replace(/(\/|^)(sass|scss)(\/|$)/gi, '$1css$3');
                if (verbose) {
                    console.log(`Moving ${path.basename}${path.extname} to ${path.dirname}`);
                }
            })
        )
        .pipe(sourcemaps.write('.'))
        .pipe(dest(paths.dest.base))
        .pipe(
            gulpIf(
                !inProduction() && tasks.styleGuide,
                rename((path) => {
                    console.log(path);
                    // Replace the styleguide dest paths to root
                    path.dirname = '/';
                    if (verbose) {
                        console.log(`Moving to styleguide ${path.basename}${path.extname} to ${paths.dest.styleguide}/css`);
                    }
                })
            )
        )
        .pipe(gulpIf(!inProduction() && tasks.styleGuide, dest(`${paths.dest.styleguide}/css`)))
        .on('end', done);
};
