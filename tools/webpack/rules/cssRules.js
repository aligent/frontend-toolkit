const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const options = require('../../options');
const { inProduction } = require('../../env');
const sass = require('sass');
const fiber = require('fibers');

module.exports = {
    test: /\.(sass|scss|pcss|css)$/,
    use: [
        !inProduction()
            ? 'style-loader'
            : {
                loader: MiniCssExtractPlugin.loader,
                options: {
                    // only enable hot in development
                    hmr: !inProduction(),
                    // if hmr does not work, this is a forceful method.
                    reloadAll: true
                }
            },
        {
            loader: 'css-loader',
            options: {
                importLoaders: 3,
                sourceMap: true
            }
        },
        {
            loader: 'postcss-loader'
        },
        {
            loader: 'resolve-url-loader',
            options: {
                debug: true,
                root: options.get('paths').webpack.sassUrlRoot
            }
        },
        {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              implementation: sass, // Prefer `dart-sass`
              sassOptions: Object.assign(options.get('sassOptions'), {
                fiber
              })
            }
        }
    ]
};
