const webpack = require('webpack');
const { mode } = require('../../env');

module.exports = new webpack.DefinePlugin({
    'process.env': {
        NODE_ENV: JSON.stringify(mode())
    }
});
