const mapping = {
    '/app/design/frontend/Aligent/stamford/source/react/Plp/themeSettings.js': './themeSettings.js',

    '/app/design/frontend/Aligent/stamford/source/react/Plp/components/Loading': './source/react/Plp/components/Loading'
};


// !!! -----------------------------------------------------------------
// !!! This is not an example implementation that can be copied to a new project
// !!! Instead, it's merely an example of the `mapping` variable above. Please use `componentOverrideMapping.sample.js`
// !!! in the root of this repository when setting up a new project.
// !!! -----------------------------------------------------------------

export default mapping;
