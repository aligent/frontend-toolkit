# Skip To Content

This helper will hide the Skip To Content link that is added to M2 sites for screen readers.the skip links from users,
while leaving them available for screen readers and other accessibility tools

## Usage

The helper will automatically be applied when the M2 Frontend Toolkit SCSS is included in the project
