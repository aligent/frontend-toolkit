const nconf = require('nconf');
const { watch } = require('gulp');
const styleGuideBrowserSync = require('browser-sync').create('styleguide');

/**
 *
 * Build the styleguide and then open it in the browser
 *
 * This will not watch for changes.
 */

module.exports = function buildStyleguide(done) {
    const paths = nconf.get('paths');
    const tasks = nconf.get('tasks');
    const verbose = nconf.get('verbose');
    if (verbose) {
        console.log('Paths:', `${paths.dest.base}/**/css/**/*.css`);
        console.warn('Styleguide Task Enabled:', tasks.styleguide);
    }
    if (!tasks.styleguide) {
        return done();
    }
    const watcher = watch([`${paths.dest.base}/**/css/**/*.css`]);

    watcher.on('change', function(path, stats) {
      console.log(`File ${path} was changed - Reloading Styleguide BrowserSync`);
      styleGuideBrowserSync.reload();
    });

    styleGuideBrowserSync.init({
        server: `./${paths.dest.styleguide}`
    });
};
