# Reset Button

`reset-button()`

Reset background, border and cursor

## Usage

```html
<button class="my-button">Red</button>
```

```css
.my-button {
  @include reset-button();
}
```

