const _ = require('lodash');
const options = require('../options');

const env = require('../env');

const resolve = require('./resolver');
const rules = require('./rules');
const plugins = require('./plugins');
const optimization = require('./optimization');
const NormalModuleOverridePlugin = require('./plugins/NormalModuleOverridePlugin');

module.exports = function webpackConfig() {
    const paths = options.get('paths');
    const destPath = options.get('paths').webpack.output.dest;
    const entries = _.pickBy(paths.webpack.entry, v => v.length > 0);

    if (_.isEmpty(entries)) {
        console.warn('No entry points supplied, Check your paths.', paths.webpack.entry);
        return {};
    }

    const config = {
        output: {
            filename: paths.webpack.output.js.filename,
            path: destPath
        },
        entry: entries,
        resolve,
        plugins,
        optimization,
        module: {
            rules
        },
        mode: env.mode(),
        watch: !env.inProduction(),
        stats: {
            // copied from `'minimal'`
            all: true,
            modules: !env.inProduction(),
            maxModules: 0,
            errors: true,
            warnings: !env.inProduction(),
            // our additional options
            moduleTrace: !env.inProduction(),
            errorDetails: true,
            entrypoints: false,
            children: false
        },
        devtool: '#source-map'
    };

    if (options.get('paths').webpack.componentOverrideMapping) {
        if (options.get('verbose')) {
            console.log(`Using componentOverrideMapping: ${options.get('paths').webpack.componentOverrideMapping}`);
        }

        const componentOverrideMapping = require(`${process.cwd()}/${options.get('paths').webpack.componentOverrideMapping}`); // eslint-disable-line
        config.plugins.push(new NormalModuleOverridePlugin(componentOverrideMapping));
    }

    return config;
};
