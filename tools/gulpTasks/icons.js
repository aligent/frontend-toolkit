const { src, dest } = require('gulp');
const iconfont = require('gulp-iconfont');
const iconfontCss = require('gulp-iconfont-css');
const rename = require('gulp-rename');
const nconf = require('nconf');

const runTimestamp = Math.round(Date.now() / 1000);

module.exports = function iconsTask(done) {
    const fontName = nconf.get('iconFontName');
    const paths = nconf.get('paths');
    const tasks = nconf.get('tasks');

    const verbose = nconf.get('verbose');
    if (verbose) {
        console.log('Icon Paths:', paths.src.icons);
        console.warn('Icon Task Enabled:', tasks.icons);
    }

    if (!tasks.icons) {
        console.log('Icons Task disabled');
        return done();
    }
    return src(paths.src.icons, { base: paths.base })
        .pipe(
            iconfontCss({
                fontName,
                path: 'scss',
                targetPath: paths.dest.icons,
                fontPath: '../icons/'
            })
        )
        .pipe(
            iconfont({
                fontName,
                prependUnicode: true,
                formats: ['woff', 'woff2'],
                timestamp: runTimestamp // recommended to get consistent builds when watching files
            })
        )
        .pipe(
            rename((path) => {
                // Replace the source paths with destination ones
                path.dirname = path.dirname.replace(new RegExp(paths.dest.regex), paths.dest.replacement);
                if (verbose) {
                    console.log(`Moving ${path.basename}${path.extname} to ${path.dirname}`);
                }
            })
        )
        .pipe(dest(paths.dest.base))
        .on('end', done);
};
