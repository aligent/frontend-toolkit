const { src, dest } = require('gulp');
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');
const options = require('../options');

module.exports = function copyTask(done) {
    const paths = options.get('paths');
    const tasks = options.get('tasks');
    const verbose = options.get('verbose');
    if (verbose) {
        console.log('Paths:', paths.src.copy);
        console.warn('Copy Task Enabled:', tasks.copy);
    }
    if (!tasks.copy) {
        return done();
    }
    return src(paths.src.copy, { base: paths.base })
        .pipe(plumber())
        .pipe(
            rename((path) => {
                // Replace the source paths with destination ones
                path.dirname = path.dirname.replace(new RegExp(paths.dest.regex), paths.dest.replacement);
                if (verbose) {
                    console.log(`Moving ${path.basename}${path.extname} to ${path.dirname}`);
                }
            })
        )
        .pipe(dest(paths.dest.base));
};
