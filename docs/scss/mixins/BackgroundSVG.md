# Background SVG

Set the background image of an element using the desired icon

`background-svg($icon)`, where `$icon` is the name of an image in the projects images folder

## Usage

```css
button {
  // Path to image is `app/design/frontend/{Vendor}/{ThemeName}/web/images/icon/icon-minicart.svg`
  @include background-svg('icon-minicart');
  // Mixin uses fetoolkit-get-icon-path() function under the hood to resolve a name to a full path
}
```
