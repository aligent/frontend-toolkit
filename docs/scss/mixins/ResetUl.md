# Reset UL

`reset-ul()`

Reset padding, margin and list style, typically for use with UL's

## Usage

```html
<ul class="list">
    <li></li>
    <li></li>
    <li></li>
</ul>
```

```css
.list {
  @include reset-ul();
}
```

