# Timer.js

This module will allow people to set and clear timeouts in JS, without having to keep track of the ID that the browser returns when setTimeout is called.

Using this module will make for much more verbose code, without having to manage storing a timeout ID on the global scope.

## Usage

**Very simple example**

```javascript
define(['Timer'], (Timer) => {

  // This module will display a message to the user if they're taking too long to fill in an input

  document.querySelector('input').addEventListener('focus', () => {
    Timer.clearTimer('my-timer');
  });

  Timer.setTimer('my-timer', () => {
    alert('This field wasn\'t filled in fast enough');
  }, 5000);

})
```

**Better example, showing the advantage of this approach**

```javascript
// module1.js
define(['Timer'], (Timer) => {

  Timer.setTimer('my-timer', () => {
    alert('The field wasn\'t filled in fast enough');
  }, 5000);

})

// module2.js
define(['Timer'], (Timer) => {

  document.querySelector('input').addEventListener('focus', () => {
    Timer.clearTimer('my-timer');
  });

})
```

### Solution without this module

Below is how we would likely otherwise have to solve the issue, which involves setting a global scoped variable on the `window` object. The issue with this is the obvious, polluting the global scope. Does the property then have to be manually deleted once it's cleared? Does this have to be implemented in all of the places that `clearTimeout` might be called?

```javascript
define(['Timer'], (Timer) => {

  window.messageTimeout = setTimeout(() => {
    alert('This field wasn\'t filled in fast enough');
  }, 5000);

})

// module2.js
define(['Timer'], (Timer) => {
  clearTimeout(window.messageTimeout);
})
```
