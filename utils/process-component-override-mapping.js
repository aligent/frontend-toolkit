/**
 * Convert a string to an absolute path "on the system".
 * This means it will contain any root paths with a docker container (if there is one)
 *
 * @param {string} string A path to a module loaded by webpack
 * @param {string} dirname The value of `__dirname` at the location of `componentOverrideMapping.js`
 *
 * @returns {string}
 */
function getAbsolutePath(string, dirname) {
    // Anything with a leading slash will be treated as "root", so we want the path to start
    // from the root of the (Magento) project
    if (string[0] === '/') {
        return `${process.cwd()}${string}`;
    }

    // Anything with a leading stop will be considered relative, so we want the path to start
    // from the current location of this file (componentOverrideMapping.js), and then look into subfolders
    if (string[0] === '.') {
        return `${dirname}${string.substring(1, string.length)}`;
    }

    throw new Error(
        `Please start path with either '/' for root path, or './' for a relative path. Path to update: "${string}"`
    );
}

/**
 * Process the componentOverrideMapping.js file to get absolute URL's for each of the source/replacement paths
 *
 * @param {object.<string, string>} mapping The source/replacement paths for overrides
 * @param {object} options                  Options object
 * @param {object} options.dirname          The value of `__dirname` at the location of the `componentOverrideMapping.js` file
 *
 * @returns {object.<string, string>}
 */
module.exports = (mapping, options) => {
    const { dirname } = options;

    const absolutePathsMapping = {};
    Object.keys(mapping).forEach((key) => {
        const moduleToReplace = getAbsolutePath(mapping[key], dirname);
        const moduleToOverride = getAbsolutePath(key, dirname);
        absolutePathsMapping[`${moduleToOverride}`] = `${moduleToReplace}`;
    });

    return absolutePathsMapping;
}
