# Contributing to the Frontend Toolkit

When contributing to this repository, please first discuss the change you wish to make with the team in the ``#frontend`
Slack channel.

In order to keep the toolkit as maintainable, developer friendly and reusable as possible, some criteria will need
to be met before creating a pull request.

These are mostly basic things that should be done for any new piece of work, but having it in writing will help to
remind people.

## Branching

Create a branch from the `master` branch, ensuring the name of the created branch is easily identifiable

## Naming

Class naming, function names and structure of classes should all be kept consistent. Refer to existing classes for examples

## Linting

The following command should run without any errors

`./node_modules/.bin/eslint js/**/*.js`

*If something fails, but you believe the rules should be amended, then you're more then welcome to propose that change
to the group.*

## Documentation

First and foremost, whatever is added **must** be documented. Any developer, frontend or backend, should be able to
read the documentation and have a good understanding of how to use each tool, but not necessarily how the tool works.

## Testing

For Javascript classes, there will need to be an associated test for that class. Every test must be pass before
creating a pull request. To run the tests, from the root project directory,

`npm run test`

## Pull Request Process

1. Update the version of the repository in the package.json file. If you're not sure which version number to go to next,
please discuss in Slack with other frontend developers
2. Update the CHANGELOG.md file, adding short description of the changes made
3. Create the pull request on Bitbucket from your branch into the `master` branch
4. Assign the pull request to at least 2 other frontend developers for review, and only merge in once both developers have approved
5. After the pull request is merged in, create a new tag at the new location of `master`
6. Delete the remote version of the branch that was just merged in
7. Update the location of the `latest` tag. Use the following commands, or if you're uncomfortable doing this, ask for assistance
  - git tag -d latest                # delete the old local tag
  - git push github :latest          # delete the old remote tag (use for each affected remote)
  - git tag latest {commit-hash}     # create a new local tag at location (GIT SHA) of most updated version
  - git push github latest           # push new tag to remote    (use for each affected remote)
