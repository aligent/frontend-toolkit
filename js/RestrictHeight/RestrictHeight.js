define(['jquery'], ($) => {
    /**
     * Class RestrictHeight
     *
     * For documentation, see `docs/js/RestrictHeight.md`
     */
    class RestrictHeight {

        /**
         * Constructor
         *
         * @param {object}  config  Configuration options passed in through JSON
         * @param {Element} element The element that this class was bound to
         *
         * @returns {undefined}
         */
        constructor(config, element) {
            const defaultConfig = {};
            // Merge default config options and provided config
            this.config = { ...defaultConfig, ...config };
            this.$element = $(element);
            this.$toggleBtn = $(this.config.toggleBtn);

            // Grab min and max widths, if not defined, set to extreme values that shouldn't ever be met
            this.maxWidth = typeof this.config.maxWidth !== 'undefined' ? this.config.maxWidth : 9999999;
            this.minWidth = typeof this.config.minWidth !== 'undefined' ? this.config.minWidth : 0;

            // Grab the initial max height inline setting. This will likely be blank, but is used when enabling height restriction again
            this.initialMaxHeight = element.style.maxHeight;

            // If the class is set to run in "programmatic" mode, this means it's being controlled by another
            // JS class, and therefore shouldn't attach it's own event handlers
            if (this.config.programatically !== 'true') {
                this.attachListeners();
            }
        }

        /**
         * Attach event listeners to elements
         *
         * @returns {undefined}
         */
        attachListeners() {
            this.$toggleBtn.on('click', () => {
                this.toggleState();
            });
        }

        /**
         * Calculate the max-height for an element that is about to expand
         *
         * @returns {number}
         */
        calculateMaxHeight() {
            const lastElementBottomPosition = this.$element
                .find('> *')
                .last()[0]
                .getBoundingClientRect().bottom;
            const firstElementTopPosition = this.$element
                .find('> *')
                .first()[0]
                .getBoundingClientRect().top;
            const maxHeight = lastElementBottomPosition - firstElementTopPosition;

            // If the current inline maxHeight is the same as the initial, then the element is being changed to its
            // non height restricted state, so set the max-height that will allow for a smooth CSS transition to
            return this.initialMaxHeight === this.$element[0].style.maxHeight
                ? `${maxHeight + 15}px`
                : this.initialMaxHeight;
        }

        /**
         * Check if the width requirements provided (or not provided) are met by the current viewport
         *
         * @returns {boolean}
         */
        meetsWidthRequirements() {
            return window.innerWidth <= this.maxWidth && window.innerWidth >= this.minWidth;
        }

        /**
         * Toggle the state of the height restriction
         *
         * @returns {undefined}
         */
        toggleState() {
            this._throwEvent('before_aligent_restrict-height_inline-style');

            if (
                (typeof this.config.initiallyRestricted === 'undefined'
                || this.config.initiallyRestricted === 'true')
                && this.meetsWidthRequirements()
            ) {
                this.$element[0].style.maxHeight = this.calculateMaxHeight();
            }

            this._throwEvent('after_aligent_restrict-height_inline-style');

            // Wrap in a timeout to ensure the above style is rendered to the page before this the toggle class is added/removed
            window.setTimeout(() => {
                this._throwEvent('before_aligent_restrict-height_toggle');

                this.$element.toggleClass('toggle-height-restriction');
                this.$toggleBtn.toggleClass('toggle-height-restriction');

                if (this.config.initiallyRestricted === 'false' && this.meetsWidthRequirements()) {
                    // Due to it becoming too involved to implement a way to go from normally opened to height restricted,
                    // while also retaining the ability to use CSS transitions, I've had to use jQuery for the slide animation
                    this.$element.slideToggle();
                }

                this._throwEvent('after_aligent_restrict-height_toggle');
            }, 10);
        }

        /**
         *
         * @param {string} event Name of event to throw
         * @param {object} extraParams [Optional] Extra parameters to send with the event
         *
         * @private
         * @returns {undefined}
         */
        _throwEvent(event, extraParams = {}) {
            this.$element.trigger(event, extraParams);
        }

    }

    return (config, element) => {
        const restrictHeight = new RestrictHeight(config, element);
        return restrictHeight;
    };
});
