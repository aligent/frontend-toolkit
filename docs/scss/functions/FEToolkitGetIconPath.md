# FE Toolkit Get Icon Path

A function that will return the path to an icon file
Name is prefixed with `fetoolkit-` to avoid any confusion that the function might be built in to SCSS or not
This function assumes Magento 2.

`fetoolkit-get-icon-path($icon, $type: 'svg')`, where `$icon` is the name of an icon, and `$type` is the filetype.

### Options

#### $icon
*string*

The name of the icon to be used, without extension type

#### $type
*string*

\[Optional\] The type of icon file. Default: `svg`

### Returns

*string*

Relative path to the icon image file
