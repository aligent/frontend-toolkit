# Carousel

Use this class to create either a manual carousel (user has to click on the next/previous buttons) or an automatic
carousel (items will automatically change to the next, but users can still click the next/previous buttons to change
what is showing).

The class name `carousel-item-active` is toggled between the carousel items.

Visibility of the items is up to you, and is to be controlled via CSS. This allows you to easily add transition animations as well

## Parameters

*N.B. All parameters are optional*

* **prevBtn** {string} *[Default] .carousel-prev*
  CSS selector for the button/element to use as the trigger to move to the previous item

* **nextBtn** {string} *[Default] .carousel-next*
  CSS selector for the button/element to use as the trigger to move to the next item

* **items** {string} *[Default] .carousel-items*
  CSS selector for the <ul> list of carousel items

* **autoplay** {Boolean} *[Default] false*
  Indicate if the carousel should auto change

* **delay** {number} *[Default] 5000*
  The amount of time, in ms, to wait before changing to the next item. Only used if `autoplay` = true

## Usage

```html
<div class="carousel-container" data-mage-init='{"carousel":{}}'>
   <button class="previous-button carousel-prev"></button>
   <ul class="items carousel-items">
       <li class="carousel-item-active">Item 1</li>
       <li>Item 2</li>
       <li>Item 3</li>
   </ul>
   <button class="next-button carousel-next"></button>
</div>
```

N.B. The above example passed no options, so it's using the default values for all parameters

*- OR -*

```html
<script type="text/x-magento-init">
 {
     ".carousel-container": { //The element to bind the class to
         "carousel": {
             "prevBtn": ".previous-button",
             "nextBtn": ".next-button",
             "items": ".items",
             "autoplay": true,
             "delay": 10000
         }
     }
 }
</script>
```

*- OR -*

```javascript
define(['carousel'], function(createCarousel) {
  return function() {
      const config = {
          "prevBtn": ".previous-button",
          "nextBtn": ".next-button",
          "items": ".items",
          "autoplay": true,
          "delay": 10000
      };
      const element = $('.carousel-container')
      const Carousel = createCarousel(config, element)
  }
})
```

## Events

There are 6 events that are thrown:

* `before_aligent_carousel_toggle`
   Thrown before the carousel is about to toggle from one item to the next

* `before_aligent_carousel_hide_prev`
   Thrown before the carousel is about to hide the previous item
   Providing a second parameter to jQuery's .on() function will give a jQuery object for the item being hidden
   e.g.
   ```
   $carouselEl.on('before_aligent_carousel_hide_prev', function(e, data) {
     data.previous = // the item being hidden
   })
   ```

* `after_aligent_carousel_hide_prev`
   Thrown after the carousel has hidden the previous item
   Providing a second parameter to jQuery's .on() function will give a jQuery object for the item hidden
   e.g.
   ```
   $carouselEl.on('after_aligent_carousel_hide_prev', function(e, data) {
     data.previous = // the item hidden
   })
   ```

* `before_aligent_carousel_show_next`
   Thrown before the carousel is about to show the next item
   Providing a second parameter to jQuery's .on() function will give a jQuery object for the item about to be shown
   e.g.
   ```
   $carouselEl.on('before_aligent_carousel_show_next', function(e, data) {
     data.next = // the item about to be shown
   })
   ```

* `after_aligent_carousel_show_next`
   Thrown after the carousel has hidden the previous item
   Providing a second parameter to jQuery's .on() function will give a jQuery object for the item hidden
   e.g. `$carouselEl.on('after_aligent_carousel_hide_prev', function(e, data) { //data.previous = the item hidden })`
   ```
   $carouselEl.on('after_aligent_carousel_hide_prev', function(e, data) {
     data.next = // the item shown
   })
   ```

* `after_aligent_carousel_toggle`
   Thrown after the carousel has toggled from one item to the next

## Example

For examples, see examples/js/Carousel.html
