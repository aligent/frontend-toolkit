// eslint-disable-next-line import/no-dynamic-require
require(['../Timer'], (Timer) => {
    jest.useFakeTimers();

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('Timer', () => {
        it('adds new timer to the queue and calls setTimeout', () => {
            const identifier = 'test-timer';
            Timer.setTimer(identifier, () => {}, 1000);

            expect(setTimeout).toHaveBeenCalledTimes(1);
            expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);

            expect(Timer.timers.length).toBe(1);
            expect(Timer.timers[0].identifier).toBe(identifier);
        });

        it('removes timer from queue and calls clearTimeout on timer ID', () => {
            const identifier = 'test-timer';
            const timerId = 1;

            Timer.timers = [
                {
                    identifier,
                    timerId
                }
            ];

            Timer.clearTimer(identifier);

            expect(clearTimeout).toHaveBeenCalledTimes(1);
            expect(clearTimeout).toHaveBeenLastCalledWith(timerId);

            expect(Timer.timers.length).toBe(0);
        });

        it('doesn\'t fail when asked to clear timer that doesn\'t exist', () => {
            const identifier = 'test-timer';

            // No timers have been set up

            try {
                Timer.clearTimer(identifier);
            } catch (e) {
                throw new Error('clearTimer wasn\'t able to handle non-existing timer identifier');
            }

            expect(clearTimeout).toHaveBeenCalledTimes(0);
        });

        it('throws exception when timer identifier has already been used', () => {
            // Set up an existing timer
            const identifier = 'test-timer';
            const timerId = 1;

            Timer.timers = [
                {
                    identifier,
                    timerId
                }
            ];

            // Attempt to create a new timer with the same identifier
            try {
                Timer.setTimer(identifier, () => {}, 1000);
                expect(true).toBe(false); // Above should throw exception, so this line shouldn't be executed
            } catch (e) {
                expect(e.message).toBe(`Timer identifier "${identifier}" already exists`);
            }
        });

        it('creates a singleton when required multiple times', () => {
            const timer1 = require('../Timer');

            const identifier = 'test-timer';
            const timerId = 1;
            Timer.timers = [
                {
                    identifier,
                    timerId
                }
            ];

            expect(timer1.timers.length).toBe(1);
        });
    });
});
