const cssRules = require('./cssRules');
const jsRules = require('./jsRules');
const tsRules = require('./tsRules');
const fontRules = require('./fontRules');
const imageRules = require('./imageRules');
const graphqlRules = require('./graphqlRules');

module.exports = [
  tsRules,
  jsRules,
  cssRules,
  fontRules,
  imageRules,
  graphqlRules,
];
