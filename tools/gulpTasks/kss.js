const { src, dest } = require('gulp');
const gulpkss = require('gulp-kss');
const nconf = require('nconf');

module.exports = function buildKss(done) {
    const paths = nconf.get('paths');
    const tasks = nconf.get('tasks');
    const verbose = nconf.get('verbose');
    if (verbose) {
        console.log('KSS paths:', paths.src.scss);
        console.warn('KSS styleguide Enabled:', tasks.styleguide);
        console.log('styleguide path:', `${paths.dest.styleguide}`);
    }

    if (!tasks.styleguide) {
        return done();
    }

    // Generate styleguide with templates
    return src(paths.src.scss)
        .pipe(
            gulpkss({
                kssOpts: {
                    config: './kss-config.json',
                    css: ['./css/bbnt.css']
                }
            })
        )
        .pipe(dest(paths.dest.styleguide));
};
