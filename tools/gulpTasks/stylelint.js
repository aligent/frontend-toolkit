const { src, dest } = require('gulp');
const gulpStylelint = require('gulp-stylelint');
const plumber = require('gulp-plumber');
const nconf = require('nconf');

module.exports = function runLinter() {
    const paths = nconf.get('paths');
    return src(paths.src.scss)
        .pipe(plumber())
        .pipe(
            gulpStylelint({
                reporters: [{ formatter: 'string', console: true }]
            })
        )
        .pipe(dest(paths.dest.base));
};
