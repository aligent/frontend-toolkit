const { src, dest } = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const terser = require('gulp-terser');
const gulpIf = require('gulp-if');
const { inProduction } = require('../env');
const options = require('../options');

module.exports = function buildJavascript(done) {
    const paths = options.get('paths');
    const babelConfig = options.get('babelConfig');
    const tasks = options.get('tasks');
    const verbose = options.get('verbose');
    if (verbose) {
        console.log('Paths:', paths.src.js);
        console.warn('Javascript Task Enabled:', tasks.javascript);
        console.log('Javascript options:', babelConfig);
    }
    if (!tasks.javascript) {
        return done();
    }
    return src(paths.src.js, { base: paths.base })
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(babel(babelConfig))
        .pipe(gulpIf(inProduction(), terser()))
        .pipe(sourcemaps.write('.'))
        .pipe(
            rename((path) => {
                // eslint-disable-next-line no-param-reassign
                path.dirname = path.dirname.replace(new RegExp(paths.dest.regex), paths.dest.replacement);
                if (verbose) {
                    console.log(`Moving ${path.basename}${path.extname} to ${path.dirname}`);
                }
            })
        )
        .pipe(dest(paths.dest.base))
        .on('end', done);
};
