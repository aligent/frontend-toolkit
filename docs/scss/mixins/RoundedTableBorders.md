# Rounded Table Borders

`rounded-table-borders($border, $radius)`

Create a table with all borders (inside and outside) and with a variable border radius on just the outside borders

## Usage

```html
<table class="my-table">
  <tr>
    <td>Cell 1</td>
    <td>Cell 2</td>
    <td>Cell 3</td>
  </tr>
  <tr>
    <td>Cell 4</td>
    <td>Cell 5</td>
    <td>Cell 6</td>
  </tr>
  <tr>
    <td>Cell 7</td>
    <td>Cell 8</td>
    <td>Cell 9</td>
  </tr>
</table>
```

```css
.my-table {
  @include rounded-table-borders(1px solid green, 5px);
}
```

### Result

![Result](https://i.ibb.co/84Sbmf6/Rounded-Table-Border.png)
