# Fix Whitespace

`fix-whitespace($fontSize)`, where `$fontSize` is the size of the font for the children elements

A mixin that can be used to work around the issue of browsers always adding a space character between two inline-block elements.

*N.B. This mixin will change the font size for all direct children elements to the ``$fontSize` value. If there are children that require a different font size, e.g. heading elements, then they'll need to have the font size specifically set on them*

For example, consider the following markup

```html
<ul class="wrapper">
  <li class="column"></li>
  <li class="column"></li>
  <li class="column"></li>
</ul>
```

and the styling

```css
.wrapper {
  width: 600px;
}

.column {
  display: inline-block;
  width: 200px;
}
```

*In this example, assume there is a reset that will remove margin/padding from ul/li elements, and there is content in
each column so the height doesn't need to be explicitly set*

The columns in that markup will wrap, as browsers will insert a single whitespace character between each element, meaning
3 * 200px + {browser space} != 600px;

See the bug in action on JSFiddle: [https://jsfiddle.net/denno020/1uf8rnkz/](https://jsfiddle.net/denno020/1uf8rnkz/)

## Usage

```css
.wrapper {
  @include fix-whitespace(16px);
}
```

## Browser Support

This fix will work in all modern browsers and IE11. If IE11 support isn't required, then pass through `initial` as the parameter
to the mixin for a much more robust solution
