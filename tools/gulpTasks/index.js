const scss = require('./scss');
const javascript = require('./javascript');
const webpack = require('./webpack');
const clean = require('./clean');
const stylelint = require('./stylelint');
const serviceworker = require('./serviceworker');
const copy = require('./copy');
const { flushTranslations, flushLayouts, flushBlocks } = require('./cache');

module.exports = {
    scss,
    javascript,
    webpack,
    flushTranslations,
    flushLayouts,
    flushBlocks,
    clean,
    stylelint,
    copy,
    serviceworker
};
