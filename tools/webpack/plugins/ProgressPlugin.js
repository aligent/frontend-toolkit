const webpack = require('webpack');

let PERCENT;

module.exports = new webpack.ProgressPlugin({
    entries: true,
    modules: true,
    profile: true,
    handler: (percentage, message, ...args) => {
        // track percent by 10s for less logs
        newPercent = Math.round((Math.ceil(percentage * 100 / 10) * 10));
        if(PERCENT !== newPercent) {
            PERCENT = newPercent;
            // using console for better support in headless envs.
            console.log(
                `${PERCENT}% ${message}`,
                `${args.length > 0 ? `${args[0]} - ` : ''}`,
                `${args.length > 0 ? `${args[1]}` : ''}`
            );
        }
    }
});
