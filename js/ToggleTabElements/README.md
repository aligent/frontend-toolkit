# Toggle Tab Elements

Class to handle showing/hiding multiple tabbed elements

Will toggle two classes (`tab-active-heading` and `tab-active`) on the heading and content elements of the tabs.
To actually show/hide the element, CSS will need to be written. This means that any desired animations can be created.

You're free to interpret the class as you wish. It can either be used to hide the element (therefore visible by default)
or it could be used to show the element (therefore hidden by default), or using media queries, you could have the class
act differently depending on the users device width (i.e. mobile or not)

## Parameter

* `tabContentClass` {string}
  A DOM selector for the tab content being toggled by the tabs

## Usage

```html
<div class="tabs-container" data-mage-init='{"toggleTabElements": {"tabContentClass": ".tab-block"}}'>
 <a href="#tab01">First tab content somewhere in the DOM</a>
 <div id="tab01" class="tab-block">First tab content somewhere in the DOM</div>
 <a href="#tab02">Second tab content somewhere in the DOM</a>
 <div id="tab02" class="tab-block">First tab content somewhere in the DOM</div>
</div>

// Add the following somewhere in your CSS. Specificity is up to you.
.tab-active {
  display: block; // You can choose how you want the element to be shown when the class is present. This essentially means visible by default
}
.tab-active-heading {
  // Style according to design
}
```

*- OR -*

```html
<script type="text/x-magento-init">
   {
       ".tabs-container": {
           "toggleTabElements": {
               "tabContentClass": ".tab-block"
           }
       }
   }
</script>
```

*- OR -*

```javascript
define(['toggleTabElements'], function(createToggleTabElements) {
 return function() {
     var config = {
         "tabContentClass": ".tab-block"
     };
     var element = $('.tabs-container')
     var ToggleTabElements = createToggleTabElements(config, element)
 }
})
```

## Events

There are 4 events that are thrown. The four events are:

* `before_aligent_tab_hide`
  Thrown before the tab hide is executed

* `after_aligent_tab_hide`
  Thrown after the tab hide is executed

* `before_aligent_tab_show`
  Thrown before the tab show is executed

* `after_aligent_tab_show`
  Thrown after the tab show is executed
