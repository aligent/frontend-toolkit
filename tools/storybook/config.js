const { configure } = require('@storybook/react');

require('./config__addon--options');

const req = require.context('../../../../app/design/frontend', true, /\.stories\.jsx?$/);

function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
