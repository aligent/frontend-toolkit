# Normalize

A modern alternative to CSS resets

Based on [https://github.com/necolas/normalize.css](https://github.com/necolas/normalize.css)
