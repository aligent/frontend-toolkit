const { watch, series, parallel } = require('gulp');

const toolkit = require('@aligent/frontend-toolkit');

// Uncomment below if using component overrides feature and dynamic theme names
// const theme = process.argv
//     .find((argv) => argv.includes('--theme'))
//     .split('=')[1];

toolkit.options.set(
    'paths:webpack:output:dest',
    `${process.cwd()}/app/design/frontend/[Vendor]/[Theme]/web/`
);

// Uncomment below if using component overrides feature
// toolkit.options.set(
//     'paths:webpack:componentOverrideMapping',
//     `./app/design/frontend/[Vendor]/[theme]/componentOverrideMapping.js`
// );

const paths = toolkit.options.get('paths');

/*
 Fetch and change options, paths via direct get(), set() calls
 or via the toolkitConfig.json in your project root

 Add non-core tasks too - just make sure you install the task devDeps into your project.
 const styleguide = require('@aligent/frontend-toolkit/tools/gulpTasks/styleguide');
 exports.styleTask = styleguide;
 */


exports.default = parallel(toolkit.tasks.javascript, toolkit.tasks.scss, toolkit.tasks.webpack);

const watcher = (done) => {
    watch(paths.src.scss, toolkit.tasks.scss);
    watch(paths.src.js, toolkit.tasks.javascript);
    done();
};

exports.watchTask = series(toolkit.tasks.javascript, toolkit.tasks.scss, watcher, toolkit.tasks.webpack);

exports.javascriptTask = toolkit.tasks.javascript;
exports.scssTask = toolkit.tasks.scss;
exports.webpackTask = toolkit.tasks.webpack;
exports.stylelintTask = toolkit.tasks.stylelint;
exports.copyTask = toolkit.tasks.copy;
