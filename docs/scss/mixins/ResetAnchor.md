# Reset Anchor

`reset-anchor()`

Reset font colour, text-decoration, outline, hover and active styles

## Usage

```html
<a class="link"></a>
```

```css
.link {
  @include reset-anchor();
}
```

