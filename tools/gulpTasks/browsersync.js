const nconf = require('nconf');
const siteBrowserSync = require('browser-sync').create('site');

const projectUrl = nconf.get('projectUrl');
const tasks = nconf.get('tasks');
const { inProduction } = require('../env');

const options = {};

if (projectUrl) {
    options.proxy = projectUrl;
}

if (!inProduction() && tasks.browsersync) {
    siteBrowserSync.init(options);
}

module.exports = siteBrowserSync;
