# Show Hide Element

Class to handle showing/hiding an element

Will simply toggle a class (`show-hide--toggle`) on an element. To actually show/hide the element, CSS will need to be written. This means that any desired animations can be used.

You're free to interpret the class as you wish. It can either be used to hide the element (therefore visible by default)
or it could be used to show the element (therefore hidden by default), or using media queries, you could have the class
act differently depending on the users device width (i.e. mobile or not)

*N.B. This class will only select the first elementToToggle that matches the DOM selector, so you will need to ensure it
is specific enough to grab the exact element that you want*

## Parameters

* **elementToToggle** {string}
  A DOM selector for the element that will have the show/hide class toggled on it
* **elementDirection** *[Optional] *
  String representing where the element to toggle is in relation to the element that this class was instantiated upon. *Options are:*
   * **self**      Select the element that this class was instantiated upon
   * **parent**    Select a parent of the element (will recursively work up the parent tree until match found)
   * **child**     Select a child of the element. This is also recursive, doesn't have to be direct child
   * **sibling**   Select a sibling. This will be an element that shares the same parent
   * **[default]** If no `elementDirection` is supplied, then the selector will be queried against the document

## Usage

```html
<button class="some-button" data-mage-init='{"showHideElement": {"elementToToggle": ".element-to-show-hide"}}'></button>
...
<div class="element-to-show-hide">Content somewhere else in the DOM</div>

// Add the following somewhere in your CSS. Specificity is up to you.
.show-hide--toggle {
  display: none; // You can choose how you want the element to be hidden. This essentially means visible by default
}
```

*- OR -*

```html
<script type="text/x-magento-init">
{
    ".some-button": { //The element to bind the class to
        "showHideElement": {
            "elementToToggle": ".element-to-show-hide",
            "elementDirection": "[blank]|self|parent|child|sibling"
        }
    }
}
</script>
```

*- OR -*

```javascript
define(['showHideElement'], function(createShowHideElement) {
 return function() {
     var config = {
         "elementToToggle": ".element-to-show-hide",
         "elementDirection": "[blank]|self|parent|child|sibling"
     };
     var element = $('.some-button');
     var ShowHideElement = createShowHideElement(config, element);
 }
})
```

## Events

There are 6 events that are thrown, with 4 thrown at any one time:

* `before_aligent_showhide_toggle`
  Thrown before the toggle is executed

* `after_aligent_showhide_toggle`
  Thrown after the toggle is executed

* `before_aligent_showhide_hide`
  Thrown before a hide toggle is executed

* `after_aligent_showhide_hide`
  Thrown after a hide toggle is executed

* `before_aligent_showhide_show`
  Thrown before a show toggle is executed

* `after_aligent_showhide_show`
  Thrown after a show toggle is executed

## Example

For examples, see examples/js/ShowHideElement.html
