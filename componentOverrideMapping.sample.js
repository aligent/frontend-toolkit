const mapping = {
    // '/app/design/frontend/[Vendor]/Base/source/react/[Variation]/components/Loading.jsx':
    //  './source/react/[Variation]/components/Loading.jsx'
};


// !!! -----------------------------------------------------------------
// !!! There should be no need to edit anything below this line
// !!! If you find that there is an issue with the below configuration, and that it needs to be updated in your project,
// !!! please consider porting that back to the FE Toolkit, and ensuring that it will work for all projects.
// !!! -----------------------------------------------------------------

const processMap = require('@aligent/frontend-toolkit/utils/process-component-override-mapping');

module.exports = processMap(mapping, { dirname: __dirname });
