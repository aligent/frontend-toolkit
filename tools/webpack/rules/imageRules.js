module.exports = {
    test: /\.(jpe?g|png|gif|svg|webp)$/i,
    use: [
        { loader: 'url-loader', options: { limit: 10000 } },
        { loader: 'img-loader', options: { filename: 'images/[name].[ext]' } }
    ]
};
