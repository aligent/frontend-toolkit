/*eslint-disable*/
const { expect } = require('chai');

describe('Carousel', () => {
    let page;

    /**
     * Before each test, load the example page in a new tab
     */
    before (async () => {
        page = await browser.newPage();
        await page.goto('http://127.0.0.1:8080/js/Carousel/example.html');
    });

    /**
     * Close the page after each test has completed
     */
    after (async () => {
        await page.close();
    });

    it('will go to the next carousel item', async () => {
        const nextBtn = await page.$('.carousel-basic .next');
        await nextBtn.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const container = document.querySelector(".carousel-container .item-2");
            return container.className.split(' ');
        });

        expect('carousel-item-active').to.be.oneOf(classNames);
    });

    /**
     * Click the previous button twice, once to cancel out the button press in the first test, and
     * a second time to roll the carousel around to the end
     */
    it('will go to the previous carousel item', async () => {
        const prevBtn = await page.$('.carousel-basic .prev');
        await prevBtn.click();
        await prevBtn.click();

        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const container = document.querySelector(".carousel-container .item-3");
            return container.className.split(' ');
        });

        expect('carousel-item-active').to.be.oneOf(classNames);
    });

    it('will set data-src value of images with data-src to image src property when showing next carousel item', async () => {
        const nextBtn = await page.$('.carousel-lazyload .next');
        await nextBtn.click();

        // Get the image src prop after clicking next
        const imageSrc = await page.evaluate(() => {
            return document.querySelector('.carousel-lazyload .item-2 img').getAttribute('src');
        });

        expect(imageSrc).to.not.be.null;
    });

    /**
     * Skipped test as need to find a reliable way to mock the setInterval JS function
     */
    it.skip('automatically advances carousel with autoplay', async function () {
        // Get the class names added to the container element
        const classNames = await page.evaluate(() => {
            const container = document.querySelector(".carousel-autoplay .item-1");
            return container.className.split(' ');
        });

        expect('carousel-item-active').to.be.oneOf(classNames);

        const classNames2 = await page.evaluate(() => {
            const container = document.querySelector(".carousel-autoplay .item-2");
            return container.className.split(' ');
        });

        expect('carousel-item-active').to.be.oneOf(classNames2);
    });
});
