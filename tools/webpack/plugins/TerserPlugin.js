const TerserPlugin = require('terser-webpack-plugin');
const options = require('../../options');
const terserOptions = options.get('terserOptions');

module.exports = new TerserPlugin(terserOptions);
