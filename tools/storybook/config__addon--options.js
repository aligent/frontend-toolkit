import { addParameters } from '@storybook/react';

// Option defaults:
addParameters({
    options: {
        name: 'Munro Storybook',
        /**
         * show story component as full screen
         * @type {Boolean}
         */
        isFullScreen: false,
        /**
         * display panel that shows a list of stories
         * @type {Boolean}
         */
        showNav: true,
        /**
         * display panel that shows addon configurations
         * @type {Boolean}
         */
        showPanel: false,
        /**
         * where to show the addon panel
         * @type {String}
         */
        panelPosition: 'bottom',
        /**
         * sorts stories
         * @type {Boolean}
         */
        sortStoriesByKind: false,
        /**
         * regex for finding the hierarchy separator
         * @example:
         *   null - turn off hierarchy
         *   /\// - split by `/`
         *   /\./ - split by `.`
         *   /\/|\./ - split by `/` or `.`
         * @type {Regex}
         */
        hierarchySeparator: /\/|\./,
        /**
         * regex for finding the hierarchy root separator
         * @example:
         *   null - turn off multiple hierarchy roots
         *   /\|/ - split by `|`
         * @type {Regex}
         */
        hierarchyRootSeparator: /\||\:/,
        /**
         * sidebar tree animations
         * @type {Boolean}
         */
        sidebarAnimations: false,
        /**
         * enable/disable shortcuts
         * @type {Boolean}
         */
        enableShortcuts: true,
        /**
         * theme storybook, see link below
         */
        theme: undefined
    }
});
