# Placeholder

`placeholder{ /* content */ }`

A mixin to make it easy to style the placeholder element cross browser

## Usage

```css
.text-input {
  @include placeholder {
    // Custom styling goes here
  }
}
```
