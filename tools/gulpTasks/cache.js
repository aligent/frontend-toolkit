const nconf = require('nconf');
const { execSync } = require('child_process');

exports.flushTranslations = async function flushTranslations(done) {
    const envEntryCmd = nconf.get('envEntryCmd');
    const command = 'bin/magento cache:flush translate full_page';
    try {
        const message = await execSync(`${envEntryCmd} "${command}"`).toString();
        console.log(message);
    } catch (e) {
        console.error(e);
    }
    done();
};

exports.flushBlocks = async function flushBlocks(done) {
    const envEntryCmd = nconf.get('envEntryCmd');
    const command = 'bin/magento cache:flush block_html full_page';
    try {
        const message = await execSync(`${envEntryCmd} "${command}"`).toString();
        console.log(message);
    } catch (e) {
        console.error(e);
    }
    done();
};

exports.flushLayouts = async function flushLayouts(done) {
    const envEntryCmd = nconf.get('envEntryCmd');
    const command = 'bin/magento cache:flush layout full_page';
    try {
        const message = await execSync(`${envEntryCmd} "${command}"`).toString();
        console.log(message);
    } catch (e) {
        console.error(e);
    }
    done();
};
