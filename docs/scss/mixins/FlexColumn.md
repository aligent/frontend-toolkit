# Flex Column

`flex-column($width, $mobile-breakpoint)`, where

* *$width*             The minimum width of the column
* *$mobile-breakpoint* The width at which the column will revert to 100% width

Make a container a column with a specified width

If the parameter passed in doesn't contain a unit, i.e. it's a fraction or decimal, then it will be converted
to it's equivalent percentage

Below the $mobile-breakpoint, columns revert to 100% width

The parent element needs the class `display: flex;` applied for the below flex properties to be applied.

## Usage

```html
<main class="container">
    <aside class="sidebar"></aside>
    <section class="content"></section>
</main>
```

```css
.sidebar {
  @include flex-column(1/4, 500px);
}
.content {
  @include flex-column(3/4, 500px);
}
```

