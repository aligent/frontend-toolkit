/**
 *
 * @typedef {object} Timer
 * @property {string} identifier
 * @property {number} timerId
 *
 */

define([], () => {
    let instance;

    /**
     * @class Timer
     */
    class Timer {

        /**
         * Array to keep track of the active timers
         *
         * @type {Timer[]}
         */
        timers = [];

        /**
         * Check if the timers identifier already exists
         *
         * @param {string} identifier The string identifier for a possible new timer
         *
         * @returns {undefined}
         */
        checkIdentifierUnique(identifier) {
            const existingTimer = this.timers.some(timer => timer.identifier === identifier);
            if (existingTimer) {
                throw new Error(`Timer identifier "${identifier}" already exists`);
            }
        }

        /**
         * Create a new setTimeout timer
         *
         * @param {string}   identifier A string identifier for this timeout. Used to later clear the timeout
         * @param {Function} fn         Function to call when delay has elapsed
         * @param {number}   delay      The amount of time to wait before calling callback function (in ms)
         *
         * @returns {undefined} Doesn't return timerID, as the timer should be cleared through this class if it's created here.
         *                 Also doesn't return identifier, as the dev should store it in a local variable where this function
         *                 is called if it's going to be needed elsewhere
         */
        setTimer(identifier, fn, delay) {
            this.checkIdentifierUnique(identifier);

            const timerId = setTimeout(() => {
                fn(); // Call users' function
                this.clearTimer(identifier);
            }, delay);

            this.timers.push({
                identifier,
                timerId
            });
        }

        /**
         * Remove a timer from the stack
         *
         * This will be called when the timer is either finished, and the callback function has run,
         * or when the user clears the timeout manually
         *
         * @param {string} identifier The string identifier given to the timer, but the developer
         *
         * @returns {undefined}
         */
        clearTimer(identifier) {
            // Ensure the timeout has been cleared
            const timerToClear = this.timers.find(timer => timer.identifier === identifier);
            // If the timer identification exists, be sure to clear the timeout
            if (timerToClear) {
                clearTimeout(timerToClear.timerId);
            }

            // Remove the cleared timer from the timers array
            const newTimers = this.timers.filter(timer => timer.identifier !== identifier);
            this.timers = newTimers;
        }

    }

    if (typeof instance === 'undefined') {
        instance = new Timer();
    }

    return instance;
});
