module.exports = {
    extends: '@aligent/babel-preset',
    env: {
        test: {
            plugins: [
                'transform-amd-to-commonjs'
            ]
        }
    }
};
