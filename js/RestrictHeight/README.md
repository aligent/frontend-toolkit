# RestrictHeight

Restrict the height of an element, hiding the rest behind a "show more" button of some sort

The class will toggle a class name, `.toggle-height-restriction`, which can be targetted in CSS to apply/remove
necessary height restrictions and transitions

In order to allow for smooth height transitions from a fixed number to an "auto" number, the class will dynamically
determine the height of the container when all children are showing, and apply that to the container as the max height.
This means that in your CSS, you only have to worry about the starting value, and effectively think of the other value
as 'auto'.

## Parameters
*N.B. Parameters that show a default value are optional*

* **toggleBtn** {string}
  CSS selector for the element that will toggle the height restriction

* **initiallyRestricted** {Boolean} *[Default] true*
  Boolean indicating the starting state of the element the height restriction will affect

* **minWidth** {number} *[Default] 0*
  The minimum width, inclusive, in px, of the viewport for the class to take effect For example, enforce the class to only work on large devices

* **maxWidth** {number} *[default] 9999999*
  The maximum width, inclusive, in px, of the viewport for the class to take effect. For example, enforce the class to only work on small devices

* **programatic** {string} *[Default] false*
  Flag to indicate if the instance is going to be controlled by outside JS


## Usage

```html
<dl class="product-specs-list" data-mage-init='{"restrictHeight":{"toggleBtn":".btn__show-all"}}'>
  <dt>Type</dt>
  <dd>Quad-Copter</dd>
  <dt>Main Rotor Diameter</dt>
  <dd>1.97 in (50.0mm)</dd>
  <dt>Gross Weight</dt>
  <dd>0.58 oz (16.5 g)</dd>
  <dt>Length</dt>
  <dd>5.5 in (140mm)</dd>
</dl>
<button class="btn__show-all">Show All</button>
```
*- OR -*

```html
<script type="text/x-magento-init">
{
  "restrictHeight": {
    ".product-specs-list": {
      "toggleBtn": ".btn__show-all",
      "initiallyRestricted": "true",
      "maxWidth": "400"
    }
  }
}
</script>
```

*- OR -*

```javascript
define(['restrictHeight'], function(createRestrictHeight) {
 return function() {
     const config = {
         "toggleBtn": ".btn__show-all",
         "initiallyRestricted": "false",
         "minWidth": "401",
         "programatic" : "true"
     };
     const element = $('.product-specs-list');
     const RestrictHeight = createRestrictHeight(config, element);
 }
})
```

## Events

There are 4 events that are thrown:

* `before_aligent_restrict-height_inline-style`
  Thrown before the inline maxHeight style is added/removed from the element


* `after_aligent_restrict-height_inline-style`
  Thrown after the inline maxHeight style is added/removed from the element


* `before_aligent_restrict-height_toggle`
  Thrown before the toggle class name is added/remove from the element


* `after_aligent_restrict-height_toggle`
  Thrown after the toggle class name is added/remove from the element
